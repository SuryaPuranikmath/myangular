import { Component, OnInit } from '@angular/core';
import { EpmloyeeService } from '../epmloyee.service';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {
  employees = [];

  // emp = [
  //   { 'name': 'surya'},
  //   { 'name': 'geeta'},
  //   { 'name': 'meena'},
  //   { 'name': 'pramod'},
  // ];
  constructor(private _employeeService: EpmloyeeService ) {

   }

  ngOnInit() {
    this.employees = this._employeeService.getEmp();
  }

}
