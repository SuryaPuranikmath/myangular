import { Component } from '@angular/core';
import { EpmloyeeService } from './epmloyee.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
   providers : [ EpmloyeeService]
})
export class AppComponent {
  title = 'FirstNgApp';
}
